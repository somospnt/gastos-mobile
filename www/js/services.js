angular.module('starter.services', ['starter.config', 'ngCordova'])

        // DB wrapper
        .factory('DB', function ($q, DB_CONFIG, $cordovaSQLite, $interval) {
            var self = this;
            self.db = null;

            self.init = function (with_data, drop_tables) {
                // Use self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name}); in production
//                self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);
                self.db = $cordovaSQLite.openDB({name: DB_CONFIG.name});

                angular.forEach(DB_CONFIG.tables, function (table) {
                    var columns = [];
                    var foreign_keys = [];

                    angular.forEach(table.columns, function (column) {
                        columns.push(column.name + ' ' + column.type);
                    });
                    angular.forEach(table.foreign_keys, function (foreign_key) {
                        foreign_keys.push('FOREIGN KEY(' + foreign_key.column + ') REFERENCES ' + foreign_key.foreign_table + '(' + foreign_key.foreign_column + ')');
                    });

                    if (drop_tables) {
                        var query = 'DROP TABLE IF EXISTS ' + table.name;
                        self.querySQLite(query);
                    }
                    var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ((foreign_keys.length > 0) ? ', ' + foreign_keys.join(',') : '') + ')';
                    self.querySQLite(query);
                    console.log('Table ' + table.name + ' initialized');
                });

                if (with_data) {
                    self.querySQLite("DELETE FROM gm_usuario");
                    self.querySQLite("DELETE FROM gm_concepto");
                    angular.forEach(DB_CONFIG.data, function (table) {
                        var query = "INSERT INTO " + table.name;
                        if (table.name === 'gm_usuario') {
                            query = query + " (id, nombre, email, avatar, fecha_alta) VALUES (?,?,?,?,?)";
                        } else if (table.name === 'gm_concepto') {
                            query = query + " (id, id_usuario, nombre, icono, fecha_alta, monto, frecuencia) VALUES (?,?,?,?,?,?,?)";
                        }

                        angular.forEach(table.values, function (row) {
                            var values = [];
                            for (var key in row) {
                                values.push(row[key]);
                            }
                            self.querySQLite(query, values);
                        });

                        console.log(table.name + ' loaded');
                    });
                }
            };

            self.querySQLite = function (query, values) {
                var wrapper = function () {
                    console.log("EXCUTING -> " + query + ((values) ? " WITH VALUES [" + values.join(",") + "]" : ""));

                    var deferred = $q.defer();


                    $cordovaSQLite.execute(self.db, query, values).then(function (res) {
                        deferred.resolve(res);
                    }, function (err) {
                        deferred.reject(err);
                        console.error(err);
                    });
                    return deferred.promise;
                }

                if (!self.db) {
                    var q = $q.defer();
                    $interval(function () {
                        if (self.db) {
                            q.resolve();
                        }
                    }, 100, 50);
                    return q.promise.then(function () {
                        return wrapper();
                    });
                }
                return wrapper();
            }

            self.query = function (query, bindings) {
                bindings = typeof bindings !== 'undefined' ? bindings : [];
                var deferred = $q.defer();

                self.db.transaction(function (transaction) {
                    transaction.executeSql(query, bindings, function (transaction, result) {
                        deferred.resolve(result);
                    }, function (transaction, error) {
                        deferred.reject(error);
                    });
                });

                return deferred.promise;
            };

            self.fetchAll = function (result) {
                var output = [];

                for (var i = 0; i < result.rows.length; i++) {
                    output.push(result.rows.item(i));
                }

                return output;
            };

            self.fetch = function (result) {
                return result.rows.item(0);
            };

            return self;
        })

        // Resource services
        .factory('Usuario', function (DB) {
            var self = this;

            self.all = function () {
                return DB.querySQLite('SELECT * FROM gm_usuario')
                        .then(function (result) {
                            return DB.fetchAll(result);
                        });
            };

            self.getById = function (id) {
                return DB.querySQLite('SELECT * FROM gm_usuario WHERE id = ?', [id])
                        .then(function (result) {
                            return DB.fetch(result);
                        });
            };

            return self;
        })

        .factory('Concepto', function (DB) {
            var self = this;

            self.all = function () {
                return DB.querySQLite('SELECT * FROM gm_concepto')
                        .then(function (result) {
                            return DB.fetchAll(result);
                        });
            };

            self.getById = function (id) {
                return DB.querySQLite('SELECT * FROM gm_concepto WHERE id = ?', [id])
                        .then(function (result) {
                            return DB.fetch(result);
                        });
            };

            return self;
        })

        .factory('Gasto', function (DB) {
            var self = this;

            self.all = function () {
                return DB.querySQLite('SELECT * FROM gm_gasto')
                        .then(function (result) {
                            return DB.fetchAll(result);
                        });
            };

            self.getById = function (id) {
                return DB.querySQLite('SELECT * FROM gm_gasto WHERE id = ?', [id])
                        .then(function (result) {
                            return DB.fetch(result);
                        });
            };

            return self;
        });