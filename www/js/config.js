angular.module('starter.config', [])
        .constant('DB_CONFIG', {
            name: 'gm.db',
            tables: [
                {
                    name: 'gm_usuario',
                    columns: [
                        {name: 'id', type: 'integer primary key'},
                        {name: 'nombre', type: 'text'},
                        {name: 'email', type: 'text'},
                        {name: 'avatar', type: 'text'},
                        {name: 'fecha_alta', type: 'integer'}
                    ]
                },
                {
                    name: 'gm_concepto',
                    columns: [
                        {name: 'id', type: 'integer primary key'},
                        {name: 'id_usuario', type: 'integer'},
                        {name: 'nombre', type: 'text'},
                        {name: 'icono', type: 'text'},
                        {name: 'fecha_alta', type: 'integer'},
                        {name: 'monto', type: 'real'},
                        {name: 'frecuencia', type: 'text'}
                    ],
                    foreign_keys: [
                        {column: 'id_usuario', foreign_table: 'gm_usuario', foreign_column: 'id'}
                    ]
                },
                {
                    name: 'gm_gasto',
                    columns: [
                        {name: 'id', type: 'integer primary key'},
                        {name: 'id_usuario', type: 'integer'},
                        {name: 'fecha_alta', type: 'integer'},
                        {name: 'concepto', type: 'text'},
                        {name: 'monto', type: 'real'}
                    ],
                    foreign_keys: [
                        {column: 'id_usuario', foreign_table: 'gm_usuario', foreign_column: 'id'}
                    ]
                }
            ],
            data: [
                {
                    name: 'gm_usuario',
                    values: [
                        {id: '1', nombre: 'maro', email: 'maro@mail.com', avatar: 'maro.jpg', fecha_alta: 'datetime("now")'}
                    ]
                },
                {
                    name: 'gm_concepto',
                    values: [
                        {id: 1, id_usuario: 1, nombre: 'Alquiler', icono: 'ion-home', fecha_alta: 'datetime("now")', monto: '3000.0', frecuencia: 'MES'},
                        {id: 2, id_usuario: 1, nombre: 'Agua', icono: 'ion-waterdrop', fecha_alta: 'datetime("now")', monto: '80.0', frecuencia: 'MES'},
                        {id: 3, id_usuario: 1, nombre: 'Patente', icono: 'ion-model-s', fecha_alta: 'datetime("now")', monto: '1100.0', frecuencia: 'MES'},
                        {id: 4, id_usuario: 1, nombre: 'Verduleria', icono: 'ion-bag', fecha_alta: 'datetime("now")', monto: '250.0', frecuencia: 'DIA'}
                    ]
                }
            ]
        });