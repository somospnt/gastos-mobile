//var db = null;

angular.module('starter.controllers', ['starter.services'])

        .controller('DashCtrl', function ($scope, Concepto) {
            Concepto.all().then(
                    function (res) {
                        $scope.conceptos = res;
                    },
                    function (err) {
                        console.log(err);
                    });
        })

        .controller('FriendsCtrl', function ($scope, Friends) {
            $scope.friends = Friends.all();
        })

        .controller('FriendDetailCtrl', function ($scope, $stateParams, Friends) {
            $scope.friend = Friends.get($stateParams.friendId);
        })

        .controller('AccountCtrl', function ($scope, Usuario) {
            Usuario.getById(1).then(
                    function (res) {
                        $scope.usuario = res;
                    },
                    function (err) {
                        console.log(err);
                    });
        });
